execute_process(
   COMMAND python ${CMAKE_SOURCE_DIR}/doc/OCDscript.py
   OUTPUT_VARIABLE _output
   OUTPUT_STRIP_TRAILING_WHITESPACE
   )

message( "Source check returned:" )
message( ${_output} )
if(NOT ${_output} MATCHES "TESTSTATUS: GOOD")
   message(FATAL_ERROR "Source check not passed" )
endif()
