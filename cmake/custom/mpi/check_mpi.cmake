include(CheckFortranSourceCompiles)

set(MPI_FOUND FALSE)

if(ENABLE_MPI)
   if(ENABLE_CRAY_WRAPPERS)

      message("-- Use CRAY wrappers; this disables MPI detection")

      set(MPI_FOUND TRUE)

   else()

      find_package(MPI)

      if(MPI_FOUND)

         set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} ${MPI_COMPILE_FLAGS}")

         include_directories(${MPI_INCLUDE_PATH})

      else()

         message(FATAL_ERROR "-- You asked for MPI, but CMake could not find any MPI installation, check $PATH")

      endif()

   endif()
endif()

if(MPI_FOUND)

   add_definitions(-DVAR_MPI)
   set(MPI_DEFS "-DVAR_MPI")

   # test whether we are able to compile a simple MPI program
   file(READ "${CMAKE_SOURCE_DIR}/cmake/custom/mpi/test-MPI-compatibility.F90" _source)
   check_fortran_source_compiles(${_source} MPI_COMPATIBLE)

   if(NOT MPI_COMPATIBLE)
      message(FATAL_ERROR "There is a problem with the MPI compatibility - check cmake/custom/mpi/test-MPI-compatibility.F90")
   endif()

   if(NOT USE_MPIF_H)
      # test whether MPI module is compatible with compiler
      file(READ "${CMAKE_SOURCE_DIR}/cmake/custom/mpi/test-MPI-f90-mod-i4.F90" _source)
      check_fortran_source_compiles(${_source} MPI_F90_I4)
      file(READ "${CMAKE_SOURCE_DIR}/cmake/custom/mpi/test-MPI-f90-mod-i8.F90" _source)
      check_fortran_source_compiles(${_source} MPI_F90_I8)
      file(READ "${CMAKE_SOURCE_DIR}/cmake/custom/mpi/test-MPI-f08-mod-i4.F90" _source)
      check_fortran_source_compiles(${_source} MPI_F08_I4)
      file(READ "${CMAKE_SOURCE_DIR}/cmake/custom/mpi/test-MPI-f08-mod-i8.F90" _source)
      check_fortran_source_compiles(${_source} MPI_F08_I8)

      if(MPI_F08_I4 OR MPI_F08_I8)
         message("-- found mpi_f08 mod, however only the f90 module is supported, droppin back to -DUSE_MPI_MOD_F90")
         add_definitions(-DUSE_MPI_MOD_F90)
      elseif(MPI_F90_I4 OR MPI_F90_I8)
         message("-- found mpi mod, setting -DUSE_MPI_MOD_F90")
         add_definitions(-DUSE_MPI_MOD_F90)
      else()
         message("-- WARNING: mpi or mpi_f08 not found, will use mpif.h instead")
      endif()
   else()
      set(MPI_F90_I8 FALSE)
      set(MPI_F08_I8 FALSE)
      set(MPI_F90_I4 FALSE)
      set(MPI_F08_I4 FALSE)
   endif()

   if(MPI_F90_I8 OR MPI_F08_I8)
      message("-- found 64bit integer mpi module")
   elseif(MPI_F90_I4 OR MPI_F08_I4)
      if(ENABLE_64BIT_INTEGERS)
         message("-- found 32bit integer mpi module, setting -DVAR_MPI_32BIT_INT")
         add_definitions(-DVAR_MPI_32BIT_INT)
         set(USE_32BIT_MPI_INTERFACE TRUE)
      else()
         message("-- found 32bit integer mpi module")
      endif()
   else()
      message("-- WARNING: integer check not successful, assuming a 32bit mpif.h instead")
      if(ENABLE_64BIT_INTEGERS)
         add_definitions(-DVAR_MPI_32BIT_INT)
         set(USE_32BIT_MPI_INTERFACE TRUE)
      endif()
   endif()

   file(READ "${CMAKE_SOURCE_DIR}/cmake/custom/mpi/test-MPI-3-features-simple.F90" _source)
   check_fortran_source_compiles( ${_source} ENABLE_MPI3_FEATURES)

   if(ENABLE_MPI3_FEATURES)
      message("-- found an MPI 3 compatible MPI lib, setting -DVAR_HAVE_MPI3")
      add_definitions(-DVAR_HAVE_MPI3)
   endif()

endif()
