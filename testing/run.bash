#Author:  Patrick Ettenhuber, pettenhuber@gmai.com, 2015
#Purpose: Run a set of build instrcutctions stored in bash scripts in the folder hierarchy
# ./machinename/<nightly|continuous>/*bash
# which take as input the temporary folder <tmpd> where the script is executed. This script assumes
# that the build instruction will make a folder in <tmpd> which contains the name of the 
# buildinstrcution. If the test is set to "nightly" the build folder is removed immediately
#
#
#INPUT
#this has to be nightly or continuous, maybe an experimental can be added
buil=$1
#full path to the dir where the repos shall be stored
if [[ $# > 1 ]]
then
   tmpd=$2
else
   tmpd=""
fi
##################################
#host name first
host=$(hostname)

if [[ $buil != "continuous" && $buil != "nightly" && $buil != "GitlabCI" ]]
then
   echo "Only continuous and nightly are supported"
   exit 1
fi

Tmpdirmade="False"
if [[ $buil != "GitlabCI" ]]
then
   if [[ -z $tmpd ]]
   then
      echo "Tmpdir not given"
      exit 2 
   fi
   if [[ ! -d $tmpd ]]
   then
      echo "Tmpdir does not exist!! Mkdir $tmpd"
      mkdir $tmpd
      Tmpdirmade="True"
   fi
   testext=""
else
   testext="-$tmpd"
   tmpd=""
fi
#
sname=$(basename "$0")
pname="$0"
aname=$(pwd)
#
if [[ -z ${pname/"$sname"/} ]]
then
   cdashdir="$aname"
else
   cdashdir=${pname/\/"$sname"/}
fi
p=$cdashdir/$host/$buil$testext
#check if info for host exists
ret=0
if [[ ! -d $p ]]
then
   echo "Information for host \"$host\" and build \"$buil\" not found"
else

   #IF GitlabCI build, check if the current branch is blacklisted and skip if that is the case
   if [[ "$buil" == "GitlabCI"  ]]
   then
      source $cdashdir/branch_blacklist.bash
      for branch in $blacklist
      do
         if [[ $CI_BUILD_REF_NAME == *"$branch"* ]]
         then
            echo "Skip CI testing on blacklisted branch(es) *$branch*"
            exit 0
         fi
      done
   fi

   for name in $p/*bash
   do
      echo $name $tmpd
      bash $name $tmpd
      if [[ $? != 0 ]]
      then
         ret=$?
      fi

      #if nighly remove the repo from the tmp dir  
      if [[ "$buil" == "nightly" ]]
      then
         echo "rm -rf $tmpd/*"$(basename ${name/".bash"/})"*"
         rm -rf $tmpd/*"$(basename ${name/".bash"/})"*
      fi
   done
fi

if [[ $Tmpdirmade == "True" ]]
then
   echo "Removing Tmpdir: rm -rf $tmpd"
   rm -rf $tmpd
fi
exit $ret
