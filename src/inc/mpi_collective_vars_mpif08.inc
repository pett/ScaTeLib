      type(MPI_Comm),intent(in)                :: comm
      integer(tensor_mpi_kind),intent(in) :: root
      type(MPI_Datatype)            :: datatype
      integer(tensor_mpi_kind) :: rank, ierr, nMPI
      integer(tensor_long_int) :: n, chunk, first_el, datatype_size
